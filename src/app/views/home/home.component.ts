import { Component, OnInit } from '@angular/core';
import { Todo, TodoService } from '../../todo.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
	todos : Todo[];
	draft : Todo;
	warning : string;

	openID : number;
  formVisible : boolean;

  todayDate : string;

  numOverdue : number;
  numToday : number;
  completionRate : string;

  constructor(private todoService : TodoService) {
  	this.todoService.fetchTodos().subscribe(data => {
  		this.todos = data;
      this.computeOverdues();
      this.computeCompletion();
  	});
  	this.warning = "";
  	this.openID = -1;

    this.formVisible = false;
    this.todayDate = getDate();
  }



  ngOnInit(): void {
  	this.draft = this.makeNewTodo(); // will load existing todo if edit becomes a functionality
  }

  //View description of selected todo
  expandTodo(id : number){
  	if(id==this.openID){
  		this.openID = -1;
  	} else {
  		this.openID = id;	
  	}
  }

  showDescription(id : number) : boolean {
    return id==this.openID;
  }

  //Delete the Todo
  deleteTodo(id : number){
  	this.todoService.deleteTodo(id);
  }

  //Insert the filled up form.
  addTodo(){
  	if(this.draft.title==""){
  		this.warning = "Title cannot be empty.";
  		return;
  	}
  	this.warning = "";
  	this.todoService.newTodo(this.draft);
  	this.draft = this.makeNewTodo();

    this.formVisible = false;
  }

  //Toggle whether the task is complete
  markTodo(todo : Todo, completed : boolean){
  	todo.complete = !todo.complete;
  	this.todoService.completeTodo(todo.id, completed);
  }

  toggleForm(){
    this.formVisible = !this.formVisible;
  }


  /*
	Utility
  */
  toDate(stamp : any) : string{
  	if(!stamp){
  		return "";
  	}
  	let str = stamp.year + "-" + stamp.month + "-" + stamp.day;
  	let d = new Date(str);

  	return d.toString().substring(4,15);
	}
	addCaps(str : string) : string{
		return str.charAt(0).toUpperCase() + str.slice(1);
	}

	makeNewTodo(){
		let todo : Todo = new Todo();
		this.todoService.getNextID().then(id => {todo.id = id});
		todo.title = "";
		todo.body = "";
		todo.tag = "";
		todo.complete = false;
		return todo;
	}

  //read YYYY-MM-DD
  overdueStatus(stamp : any): number{
    if(!stamp){
      return 0;
    }
    //reconstruct string
    let str = formDate(stamp.year,stamp.month,stamp.day);

    //if due date later, return 0
    if(str>this.todayDate){
      return 0;
    }else if(str==this.todayDate){
      return 1; //due today return 1
    } else {
      return 2; //overdue return 2
    }
  }

  computeCompletion(){
    let str = "";

    if(!this.todos){
      this.completionRate = "Error"
      return;
    }
    let count = 0;
    this.todos.forEach(todo => {
      if(todo.complete){
        count +=1;
      }
    });
    str += count.toString();
    str += "/";
    str += this.todos.length.toString();

    this.completionRate = str;
  }

  computeOverdues(){
    this.numOverdue = 0
    this.numToday = 0

    if(!this.todos){
      return;
    }

    this.todos.forEach(todo => {
      let item = this.overdueStatus(todo.duedate);
      if(!todo.complete){
        if(item==2){ this.numOverdue += 1 }
        else if(item==1){ this.numToday += 1 }  
      }      
    });
  }  
}


function formDate(year, month, day) {
  year = year.toString();
  month = month.toString();
  day = day.toString();
  if (month.length < 2) 
        month = '0' + month;
    if (day.length < 2) 
        day = '0' + day;

  return [year, month, day].join('-');
}

function getDate() {
    var d = new Date(),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) 
        month = '0' + month;
    if (day.length < 2) 
        day = '0' + day;

    return [year, month, day].join('-');
}