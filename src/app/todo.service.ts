import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { AngularFirestore } from '@angular/fire/firestore';


@Injectable({
  providedIn: 'root'
})

/*
	Only TodoService will interact directly with Firebase
*/
export class TodoService {  
  draft : Todo;
  user : string;

  constructor(private firestore: AngularFirestore) {
  	this.user = "root";
  }

  //hold a draft
  setCurrentDraft(todo: Todo): void {
  	this.draft = todo;
  }
  getCurrentDraft(): Todo {
  	return this.draft;
  }

  /*
		Functions for interacting with DB
  */
  async getNextID(): Promise<number>{
  	let nextID : number;
  	let colls = this.firestore.collection('users').doc(this.user);
  	await colls.ref.get().then(data => {nextID = data.get("nextID")});
    
  	colls.update({ nextID : nextID + 1}); //increment ID
  	return nextID;
  }

  //Get all todos for user.
  fetchTodos(): Observable<Todo[]> {
  	let colls = this.firestore.collection('users').doc(this.user).collection<Todo>('todos');
	  return colls.valueChanges();
  }

  async newTodo(todo : Todo): Promise<void> {
  	let colls = this.firestore.collection('users').doc(this.user).collection<Todo>('todos');
  	
  	//Check if todo with that ID exists.
  	let size : number;
  	await colls.ref.where('id','==', todo.id).get().then(snap => {size = snap.size});
  	if(size>0){
  		throw new Error('Todo ID already in use');
  	}

  	//If nothing wrong, add the new todo
  	let jsonObject = JSON.parse(JSON.stringify(todo));
  	await colls.doc(todo.id.toString()).set(jsonObject);
  }

  //TODO
  async getTodo(id : number): Promise<Todo> {
  	let val;
  	let colls = this.firestore.collection('users').doc(this.user).collection<Todo>('todos');
  	await colls.doc(id.toString()).ref.get().then(data => {val = data})
  	return val.data();
  }

  async deleteTodo(id : number): Promise<void> {
  	let colls = this.firestore.collection('users').doc(this.user).collection<Todo>('todos');
  	
  	//Check if todo with that ID exists.
  	let size : number;
  	await colls.ref.where('id','==', id).get().then(snap => {size = snap.size});

  	if(size==0){
  		throw new Error('Todo does not exist.');
  	}
		colls.doc(id.toString()).delete();
  }

  async completeTodo(id : number, completed : boolean): Promise<void> {
  	let colls = this.firestore.collection('users').doc(this.user).collection<Todo>('todos');
  	//Check if todo with that ID exists.
  	let size : number;
  	await colls.ref.where('id','==', id).get().then(snap => {size = snap.size});
  	if(size==0){
  		throw new Error('Todo does not exist.');
  	}

		colls.doc(id.toString()).update({ complete : completed });
  }

}

export class Todo {
	id: number;
  title: string;
  body: string;
  duedate : Date;
  tag : string;
  complete : boolean;
}