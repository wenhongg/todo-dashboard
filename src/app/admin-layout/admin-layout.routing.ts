import { Routes } from '@angular/router';

import { HomeComponent } from '../views/home/home.component';
import { SettingsComponent } from '../views/settings/settings.component';

export const AdminLayoutRoutes: Routes = [
    { path: 'home',	component: HomeComponent },
    { path: 'settings',	component: SettingsComponent },
];
