// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,

  firebase: {
    apiKey: "AIzaSyCQtkQ6EKYteDi1AwsE2Ro1uPKAkt-LzJc",
    authDomain: "todos-744fb.firebaseapp.com",
    databaseURL: "https://todos-744fb.firebaseio.com",
    projectId: "todos-744fb",
    storageBucket: "todos-744fb.appspot.com",
    messagingSenderId: "1076574600018",
    appId: "1:1076574600018:web:f9a0f1c6d776ed48a9988b",
    measurementId: "G-1QFQYHGS2K"
  }
};