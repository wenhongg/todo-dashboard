# Todo Dashboard

A todo-list which dispenses reminders.

This application is built with styles from Creative Tim's Paper Dashboard. 

Although this is an interview assessment, I intend to continue working on it hereafter. Hence I opted to have a dashboard as I intend to add more functionality.

## DB Schema:

├── Users  
│   ├── Todos  
│   │   ├── ID  
│   │   ├── Title  
│   │   ├── Body  
│   │   ├── Due Date  
│   │   ├── Tags  
│   │   ├── isComplete  
│   ├── nextID (of todo)  

At present 'root' is the only user.

## To do

1. Implement search by tags
2. Implement more sorting functionality
3. Consider more explanations
4. Add edit task
5. Recommend tags
6. Make overdue tasks red

